.686
.model flat
extern _ExitProcess@4 : PROC
extern __write : PROC
extern __read : PROC
extern _MessageBoxA@16 : PROC

public _main

.data

liczba db 12 dup(?) ;znak, cyfry, enter
cyfry db 12 dup(?) ;znak, cyfry, enter
cyfry_spacja db 22 dup(?) ;znak, cyfra,spacja , znak/enter
obszar db  81 dup(?) ;obszar znakow podanych przez uzytkownika
cyfry_po_trzy db 15 dup (?)
tytul db 'T', 'y', 't', 'u', 'l' ;tytul do okna


.code
_main:
CALL wczytaj_liczbe
;push eax ;zeby nie popsuc se eax przez _write
;CALL wczytaj_znaki
;CALL spacja_na_znak
;CALL wyswietl_okno	;nie rozumiem dlaczego tak sie crashuje pod sam koniec, skoro jak to co w sroku dac bez calla to smiga
;push eax	
;CALL wyswietl_ze_znakiem
;pop eax
CALL wyswietl_ze_spacja
;CALL wyswietl_po_trzy
push 0
CALL _ExitProcess@4

wczytaj_liczbe PROC ;zapisuje podana liczbe z ASCII liczbowo do eax
push DWORD PTR 12 ;ze bedziemy czytac 12 znakow
push OFFSET liczba
push 0
CALL __read
add esp, 12
mov ebx, OFFSET liczba ;adres do ebx
mov ebp, 0 ;nasz "bool" do sprawdzenia czy byl znak "-"
cmp byte PTR [ebx], byte PTR '-' ;sprawdzamy czy znak minusa, nie wiem czy ten peirwszy tez??
je mamy_ujemna
powrot:
mov esi, 0 ;poprzednia cyfra/liczba - ostateczny wynik
transformacja:
mov dl, [ebx] ;pobranie znaku
cmp dl, 10 ;sprawdzam czy enter
je koniec ;jezli spelnione to wychodzimy
sub dl, 30h ;zamiana na cyfre
movzx ecx, dl ;zapisanie wartosci aktualnie "zabranej"
mov eax, 10 ;eax mnoznikiem
mul esi ;aktualna razy eax
add eax, ecx ;dodanie wynozonej poprzedniej do aktualnie zabranej
mov esi, eax ;zaktualizowanie wartosci poprzedniej - nowa bedize poprzednia w kolejnym cyklu
inc ebx ;zwiekszenie adresu o 1 -> przesuniecie o jeden bajt
jmp transformacja
koniec:
mov eax, esi ;ostateczna wartosc przepisana do eax
cmp ebp, 1
je negacja
ret
negacja:
neg eax ;negujemy calosc bo ujemna byla
ret
mamy_ujemna:
mov ebp, 1 ;zaznaczama ze mamy ujemna
inc ebx ;zwiekszamy ebx, aby nie liczyc znaku minusa
jmp powrot
wczytaj_liczbe ENDP

wyswietl_ze_znakiem PROC ;wyswitetla liczbe z eax
mov ebp,0
mov ecx, 10 ;10-liczba obiegow petlli, bo w 32 bitach kolo 4 miliard - 10 cyfr
mov ebx, 10 ;dzielnik 
mov edi, OFFSET cyfry + 10 ;przypisanie do edi adresu 10tego bitu z cyfry
or eax, eax ;ustawi nam flage znaku
js liczba_ujemna ;i teraz majac flage, skok gdy znak
powrot:
dzielenie: ;poczatek petli konwersji
mov edx, 0 ;zerowanie starszej czesci dzielnej - zeby 64 bitowa
div ebx  ;dzielenie przez 10, iloraz w EAX reszta w EDX
add dl, 30H ;zamiana cyfry - reszty na ASCII, wynik w dl bo maks reszta to 9 wiec nie trzeba calego edx
mov [edi], dl ;odeslanie cyfry do tablicy, bez podawania "cyfry" bo edi juz wyznaczylizmy jako: "OFFSET cyfry + 10"
sub edi, 1 ;zmniejszenie indeksu ;tak naprawde to odejmujemy jeden od adresu wiec jeden bajt do tylu
loop dzielenie ;sterowanie petla

mov byte PTR cyfry[11], 10 ;wpisanie znaku nowej linii po
mov byte PTR cyfry[0], '0' ;ustawienie 0 na poczatek, zeby nie przerabiac juz petli usuwajacej
mov esi, OFFSET cyfry+1 ;tak naprawde to tez naprawua i nie trzeba ^^ jka nie ebdzie chial plusa to wywalic! i 0 z gory musi zostac wtedy
mov edi, 0 ;zliczamy ile 0
mov ecx, 12 ;maks ilosc znakow
usuwanie:
mov al, [esi] ;pobranie kolejnej cyfry
cmp al, '0' ;sprawdzenie czy cyfra 0
jne dalej ;wyjscie z petli, jezeli nie rowne to skok
inc esi ;zwiekszenie indeksu o 1 - dodanie jeden do wartosci adresowej przesuniecie o jeden bajt
inc edi ;zwiekszamu counter bo wystapilo zero
jmp usuwanie ;skok na poczatek
dalej:
cmp edi, 10 ;na chama-gdy licznik 0 osiagnal maks - same zera wiec wpisane 0 - cala oddzielna regula
je gdy_zero_wpisane
dec esi ;zmniejszamy esi zeby wcisnac znak
sub ecx, edi ;odejmujemy maks znakow od ilosci zer
mov [esi], byte PTR '+'
or ebp,ebp
jz ciag_dalszy ; jezeli 0 - czylie nie byla ujemna
mov [esi], byte PTR '-' ;jezeli tak to bez bialego znaku przed, byte PTR trzeba podac bo trzeba zdefiniowac wielkosc
ciag_dalszy:
push DWORD PTR ecx ; liczba wyswietlanych znakow
push esi ; adres obszaru z cyframi, OFFSET-oznacza adres
push DWORD PTR 1 ; numer urzadzenia - 1 - ekran
CALL __write ; wyswietlenie liczby, argumenty kty wywolujemy funkcje przekzaujemy przez stos - od ostatniego!!!
add esp, 12
ret
gdy_zero_wpisane:
sub esi, 1 ;2 gdy chcemy jeszce jakis znak - konocwy i tak jest bo gdy cos innego niz 0 to "esi przerywa" - petla: dalej
push DWORD PTR 2 ;bo tlyko 0 i znak konca - enter
push esi
push DWORD PTR 1
CALL __write
add esp, 12
ret
liczba_ujemna:
neg eax ;przerabiamy na dodatnia
mov ebp,1
jmp powrot

wyswietl_ze_znakiem ENDP

wyswietl_ze_spacja PROC ;wyswietla w konwecji znak, spacja, liczba_n, spacja, liczba_n+1, spacja, ... ,znak konca/enter
mov ebp,0
mov ecx, 10 ;10-liczba obiegow petlli, bo w 32 bitach kolo 4 miliard - 10 cyfr
mov ebx, 10 ;dzielnik 
mov edi, OFFSET cyfry_spacja + 20 ;przypisanie do edi adresu 10tego bitu z cyfry
or eax, eax ;ustawi nam flage znaku
js liczba_ujemna ;i teraz majac flage, skok gdy znak
powrot:
dzielenie: ;poczatek petli konwersji
mov edx, 0 ;zerowanie starszej czesci dzielnej - zeby 64 bitowa
div ebx  ;dzielenie przez 10, iloraz w EAX reszta w EDX
add dl, 30H ;zamiana cyfry - reszty na ASCII, wynik w dl bo maks reszta to 9 wiec nie trzeba calego edx
mov [edi], dl ;odeslanie cyfry do tablicy, bez podawania "cyfry" bo edi juz wyznaczylizmy jako: "OFFSET cyfry + 10"
sub edi, 1 ;zmniejszenie indeksu ;tak naprawde to odejmujemy jeden od adresu wiec jeden bajt do tylu
mov byte PTR [edi], byte PTR 32 ;wpisujemy spacje
sub edi, 1 ;zmniejszamy-tu bedzie kolejna cyfra
loop dzielenie ;sterowanie petla

mov byte PTR cyfry_spacja[21], 10 ;wpisanie znaku nowej linii po
mov byte PTR cyfry_spacja[0], '0' ;ustawienie 0 na poczatek, zeby nie przerabiac juz petli usuwajacej
mov esi, OFFSET cyfry_spacja+1 ;tak naprawde to tez naprawua i nie trzeba ^^ jka nie ebdzie chial plusa to wywalic! i 0 z gory musi zostac wtedy
mov edi, 0 ;zliczamy ile 0 i spacji!!
mov ecx, 22 ;maks ilosc znakow!!
usuwanie:
mov al, [esi] ;pobranie kolejnej cyfry
cmp al, 32 ;sprawdzamy czy spacja
je gdyspacja
cmp al, '0' ;sprawdzenie czy cyfra 0
jne dalej ;wyjscie z petli, jezeli nie rowne to skok
gdyspacja:
inc esi ;zwiekszenie indeksu o 1 - dodanie jeden do wartosci adresowej przesuniecie o jeden bajt
inc edi ;zwiekszamu counter bo wystapilo zero
jmp usuwanie ;skok na poczatek
dalej:
cmp edi, 20 ;na chama-gdy licznik 0 osiagnal maks - same zera wiec wpisane 0 - cala oddzielna regula
je gdy_zero_wpisane
dec esi ;zmniejszamy esi zeby wcisnac znak
mov byte PTR [esi], byte PTR 32 ;wrzucamy spacje, zeby spacja przed znakiem
dec esi ;zmniejszamy jeszcze raz
dec edi ;zmniejszamy licznik bo jedna spacje jeszce chcemy po znaku i przed pierwsza cyfra
sub ecx, edi ;odejmujemy maks znakow od ilosci zer
mov [esi], byte PTR '+'
or ebp,ebp
jz ciag_dalszy ; jezeli 0 - czylie nie byla ujemna
mov [esi], byte PTR '-' ;jezeli tak to bez bialego znaku przed, byte PTR trzeba podac bo trzeba zdefiniowac wielkosc
ciag_dalszy:
push DWORD PTR ecx ; liczba wyswietlanych znakow
push esi ; adres obszaru z cyframi, OFFSET-oznacza adres
push DWORD PTR 1 ; numer urzadzenia - 1 - ekran
CALL __write ; wyswietlenie liczby, argumenty kty wywolujemy funkcje przekzaujemy przez stos - od ostatniego!!!
add esp, 12
ret
gdy_zero_wpisane:
sub esi, 2 ;2 bo checmy spacje przed zerem
push DWORD PTR 3 
push esi
push DWORD PTR 1
CALL __write
add esp, 12
ret
liczba_ujemna:
neg eax ;przerabiamy na dodatnia
mov ebp,1
jmp powrot

wyswietl_ze_spacja ENDP 

wczytaj_znaki PROC
push DWORD PTR 81 ;ze bedziemy czytac 12 znakow
push OFFSET obszar
push 0
CALL __read ;read zapisuje w eax ile znakow wiec EZ
add esp,12
ret
wczytaj_znaki ENDP

spacja_na_znak PROC ;zamienia wsyzstkie spacje w "obszar" na wybrany znak - domyslnie *
mov ecx, 81 ;maks obieg
mov esi, OFFSET obszar ;adres pierwszego bitu z obszar
zamiana:
cmp byte PTR [esi], byte PTR 32
jne nic
mov byte PTR [esi], byte PTR '*'
nic:
inc esi
loop zamiana
ret
spacja_na_znak ENDP

wyswietl_po_trzy PROC
mov ebp,0
mov esi, 0 ;licznik do 3
mov ecx, 10 ;10-liczba obiegow petlli, bo w 32 bitach kolo 4 miliard - 10 cyfr
mov ebx, 10 ;dzielnik 
mov edi, OFFSET cyfry_po_trzy + 13 ;
or eax, eax ;ustawi nam flage znaku
js liczba_ujemna ;i teraz majac flage, skok gdy znak
powrot:
dzielenie: ;poczatek petli konwersji
mov edx, 0 ;zerowanie starszej czesci dzielnej - zeby 64 bitowa
div ebx  ;dzielenie przez 10, iloraz w EAX reszta w EDX
add dl, 30H ;zamiana cyfry - reszty na ASCII, wynik w dl bo maks reszta to 9 wiec nie trzeba calego edx
mov [edi], dl ;odeslanie cyfry do tablicy, bez podawania "cyfry" bo edi juz wyznaczylizmy jako: "OFFSET cyfry + 10"
inc esi ;zwiekszamy licznik
cmp esi, 3
jne jeszcze_nie_trzy
sub edi, 1 ;zmniejszamy indeks by wrzucic spacje
mov [edi], byte PTR 32 ;wrzucamy spacje
mov esi, 0 ;zerujemy licznik
jeszcze_nie_trzy:
sub edi, 1 ;zmniejszenie indeksu ;tak naprawde to odejmujemy jeden od adresu wiec jeden bajt do tylu
loop dzielenie ;sterowanie petla

mov byte PTR cyfry_po_trzy[14], 10 ;wpisanie znaku nowej linii po
mov byte PTR cyfry_po_trzy[0], '0' ;ustawienie 0 na poczatek, zeby nie przerabiac juz petli usuwajacej
mov esi, OFFSET cyfry_po_trzy+1 ;tak naprawde to tez naprawua i nie trzeba ^^ jka nie ebdzie chial plusa to wywalic! i 0 z gory musi zostac wtedy
mov edi, 0 ;zliczamy ile 0 i spacji!!
mov ecx, 15 ;maks ilosc znakow!!
usuwanie:
mov al, [esi] ;pobranie kolejnej cyfry
cmp al, 32 ;sprawdzamy czy spacja
je gdyspacja
cmp al, '0' ;sprawdzenie czy cyfra 0
jne dalej ;wyjscie z petli, jezeli nie rowne to skok
gdyspacja:
inc esi ;zwiekszenie indeksu o 1 - dodanie jeden do wartosci adresowej przesuniecie o jeden bajt
inc edi ;zwiekszamu counter bo wystapilo zero
jmp usuwanie ;skok na poczatek
dalej:
cmp edi, 13 ;na chama-gdy licznik 0 osiagnal maks - same zera wiec wpisane 0 - cala oddzielna regula
je gdy_zero_wpisane
;dec esi ;zmniejszamy esi zeby wcisnac znak
;mov byte PTR [esi], byte PTR 32 ;wrzucamy spacje, zeby spacja przed znakiem
dec esi ;zmniejszamy jeszcze raz
;dec edi ;zmniejszamy licznik bo jedna spacje jeszce chcemy po znaku i przed pierwsza cyfra
sub ecx, edi ;odejmujemy maks znakow od ilosci zer i spacji
mov [esi], byte PTR '+'
or ebp,ebp
jz ciag_dalszy ; jezeli 0 - czylie nie byla ujemna
mov [esi], byte PTR '-' ;jezeli tak to bez bialego znaku przed, byte PTR trzeba podac bo trzeba zdefiniowac wielkosc
ciag_dalszy:
push DWORD PTR ecx ; liczba wyswietlanych znakow
push esi ; adres obszaru z cyframi, OFFSET-oznacza adres
push DWORD PTR 1 ; numer urzadzenia - 1 - ekran
CALL __write ; wyswietlenie liczby, argumenty kty wywolujemy funkcje przekzaujemy przez stos - od ostatniego!!!
add esp, 12
ret
gdy_zero_wpisane:
sub esi, 1 ;2 gdy checmy spacje przed zerem
push DWORD PTR 2 ;wtedy tu 3 jezeli^^
push esi
push DWORD PTR 1
CALL __write
add esp, 12
ret
liczba_ujemna:
neg eax ;przerabiamy na dodatnia
mov ebp,1
jmp powrot
wyswietl_po_trzy ENDP

wyswietl_okno PROC ;wyswietla messageboxaa o tytule z "tytul" i tresc z "obszar"
push 0
push OFFSET tytul ;nazawa okna, NULL - nazwa domyslna: "error"
push OFFSET obszar ;adres obszaru znakow
push 0
CALL _MessageBoxA@16
;add esp, 16 ;jka w podprogramie to crash wtf?
ret
wyswietl_okno ENDP



END